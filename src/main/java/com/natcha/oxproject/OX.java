/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natcha.oxproject;
import java.util.Scanner;
/**
 *
 * @author USER01
 */
public class OX {
    public static void main(String[] args){
        String location[][] = new String[3][3];
        int row, col;
        String turn="";
        Scanner kb = new Scanner(System.in);
        //display this informations before game start 
        System.out.println("Welcome to OX Game");
        System.out.println(" 1 2 3");
        System.out.println("1- - -");
        System.out.println("2- - -");
        System.out.println("3- - -");
        
        //add '-' in array
        for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
                location[j][k] = "-";
            }
        }
        
        //start game, input O or X
        for(int i=1; i<10; i++){
            if(i%2!=0){
                turn = "X";
                System.out.println("X turn");
            }else{
                turn = "O";
                System.out.println("O turn");
            }
            System.out.println("Please input Row Col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            
            //checking input
            while(row>3||row<1||col>3||col<1){
                System.out.println("[!]Please input Row Col again: ");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            
            //put O/X in the table & checks the empty spaces 
            if(location[row-1][col-1].equals("-")){
                location[row-1][col-1] = turn;
            }else{
                while(!location[row-1][col-1].equals("-")){
                    System.out.println("[!]Please input Row Col again: ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    //checking input again  
                    while(row>3||row<1||col>3||col<1){
                        System.out.println("[!]Please input Row Col again: ");
                        row = kb.nextInt();
                        col = kb.nextInt();
                     }                  
                }
                location[row-1][col-1] = turn;
            }
            
            //display the game on the screen 
            System.out.println(" 1 2 3");
            int num_row = 0;
            for(int j=0;j<3;j++){
                num_row++;
                System.out.print(num_row);
                for(int k=0;k<3;k++){      
                    System.out.print(location[j][k]+" ");
                }
                System.out.println();
            }
            
            //*** draw case ***
            if(i==9){
                System.out.println("Draw....");
                break;
            }
            
            //checking for finding the winner
            //h = horizontal, v = verticle, d = diagonal
            String keep_h="", keep_v="", keep_d1="", keep_d2="";
            for(int j=0;j<3;j++){
                for(int k=0,m=2;k<3;k++,m--){
                    keep_h = keep_h+location[j][k];
                    keep_v = keep_v+location[k][j];
                    keep_d1 = keep_d1+location[k][k];
                    keep_d2 = keep_d2+location[k][m];
                }
                
                //checks
                if(keep_h.equals("XXX")||keep_v.equals("XXX")||keep_d1.equals("XXX")||keep_d2.equals("XXX")){
                    System.out.println("Player X win....");
                    i=10; //break for the first loop
                    break;
                }else if(keep_h.equals("OOO")||keep_v.equals("OOO")||keep_d1.equals("OOO")||keep_d2.equals("OOO")){
                    System.out.println("Player O win....");
                    i=10; //break for the first loop
                    break;
                }else{
                    keep_h="";
                    keep_v="";
                    keep_d1="";
                    keep_d2="";
                }
            }
        }
        System.out.println("Bye bye ....");
    }
    
}
